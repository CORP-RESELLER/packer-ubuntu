#!/bin/bash -eux

echo "Installing latest Ansible version"
apt-get install -y software-properties-common
apt-add-repository -y ppa:ansible/ansible
apt-get update
apt-get install -y \
    ansible \
    python-mysqldb \
    python-pycurl \
    python-passlib

echo "Installing default packages"
apt-get install -y \
    parted \
    curl \
    zsh \
    moreutils \
    byobu \
    multitail \
    dnsutils \
    telnet \
    vim \
    tmux \
    zip \
    unzip \
    bzip2 \
    p7zip-full \
    git \
    sudo \
    cstream \
    e2fsprogs \
    xfsprogs \
    btrfs-tools \
    incron \
    jq \
    sloccount \
    pydf \
    ack-grep \
    tig \
    htop \
    atop \
    iftop \
    mytop \
    iotop \
    pfqueue \
    fuse \
    sshfs \
    xvfb \
    ldap-utils \
    build-essential \
    strace \
    closure-compiler \
    g++ \
    tshark \
    tcpflow \
    tcpdump \
    ngrep \
    swaks \
    ethtool \
    netcat-openbsd \
    mailutils \
    mysql-client \
    libsqlite3-dev \
    php5-dev \
    libcurl3-gnutls-dev \
    libxml2-dev \
    libxml2-utils \
    libcairo2-dev \
    libjpeg8-dev \
    libpango1.0-dev \
    libgif-dev \
    ruby \
    ruby-dev \
    ruby-compass \
    subversion \
    git \
    git-flow \
    poppler-utils \
    graphicsmagick \
    imagemagick \
    wkhtmltopdf \
    tnef \
    dos2unix \
    octave \
    lynx \
    links \
    mutt \
    p7zip \
    unrar \
    unace \
    colordiff \
    graphviz \
    nodejs \
    nodejs-legacy \
    node-less \
    zram-config \
    haveged \
    dnsmasq \
    apache2-mpm-worker \
    libapache2-mod-macro \
    apache2-utils \
    dovecot-core \
    dovecot-imapd \
    postfix \
    bsd-mailx \
    php5-cli \
    php5-curl \
    php5-mysqlnd \
    php5-xhprof \
    php5-memcache \
    php5-mcrypt \
    php5-gd \
    php5-sqlite \
    php5-xmlrpc \
    php5-xsl \
    php5-geoip \
    php5-ldap \
    php-pear \
    samba \
    npm

npm install gulp -g
npm install growl -g
npm install bower -g
npm install phantomjs -g
npm install typescript -g
npm install jshint -g
npm install jslint -g
npm install grunt -g


gem update --system
gem install compass

sed -i 's/GRUB_CMDLINE_LINUX_DEFAULT=""/GRUB_CMDLINE_LINUX_DEFAULT="quiet consoleblank=0"/' /etc/default/grub
update-grub